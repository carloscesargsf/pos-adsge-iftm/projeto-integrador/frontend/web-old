export default class Answer {
  constructor(id, text, correct) {
    this._id = id || null;
    this._text = text || null;
    this._correct = correct || false;
  }

  montaModel(answer) {
    this._id = answer.id || null;
    this._text = answer.text || null;
    this._correct = answer.correct || false;
  }

  set id(id) {
    this._id = id;
  }

  get id() {
    return this._id;
  }

  set text(text) {
    this._text = text;
  }

  get text() {
    return this._text;
  }

  set correct(correct) {
    this._correct = correct;
  }

  get correct() {
    return this._correct;
  }

  get json() {
    return {
      id: this._id || null,
      text: this._text || null,
      correct: this._correct || false
    };
  }
}
