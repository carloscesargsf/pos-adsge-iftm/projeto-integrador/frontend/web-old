export default class Question {
  constructor(id, statement, answers) {
    this._id = id || null;
    this._statement = statement || null;
    this._answers = answers || [];
  }

  montaModel(question) {
    this._id = question.id || null;
    this._statement = question.statement || null;
    this._answers = question.answers || [];
  }

  set id(id) {
    this._id = id;
  }

  get id() {
    return this._id;
  }

  set statement(statement) {
    this._statement = statement;
  }

  get statement() {
    return this._statement;
  }

  set answers(answers) {
    this._answers = answers;
  }

  get answers() {
    return this._answers;
  }

  get json() {
    return {
      id: this._id || null,
      statement: this._statement || null,
      answers: this._answers || []
    };
  }
}
