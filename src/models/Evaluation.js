export default class Evaluation {
  constructor(id, description, questions) {
    this._id = id || null;
    this._description = description || null;
    this._questions = questions || [];
  }

  montaModel(evaluation) {
    this._id = evaluation.id || null;
    this._description = evaluation.description || null;
    this._questions = evaluation.questions || null;
  }

  set id(id) {
    this._id = id;
  }

  get id() {
    return this._id;
  }

  set description(description) {
    this._description = description;
  }

  get description() {
    return this._description;
  }

  set questions(questions) {
    this._questions = questions;
  }

  get questions() {
    return this._questions;
  }

  get json() {
    return {
      id: this._id || null,
      description: this._description || null,
      questions: this._questions || []
    };
  }
}
