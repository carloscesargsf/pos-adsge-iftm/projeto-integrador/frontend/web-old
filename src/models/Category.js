export default class Category {
  constructor(id, name, description, pattern) {
    this._id = id || null;
    this._name = name || null;
    this._description = description || null;
    this._pattern = pattern || null;
  }

  montaModel(category) {
    this._id = category.id || null;
    this._name = category.name || null;
    this._description = category.description || null;
    this._pattern = category.pattern || null;
  }

  set id(id) {
    this._id = id;
  }

  get id() {
    return this._id;
  }

  set name(name) {
    this._name = name;
  }

  get name() {
    return this._name;
  }

  set description(description) {
    this._description = description;
  }

  get description() {
    return this._description;
  }

  set pattern(pattern) {
    this._pattern = pattern;
  }

  get pattern() {
    return this._pattern;
  }

  get json() {
    return {
      id: this._id || null,
      name: this._name || null,
      description: this._description || null,
      pattern: this._pattern || null
    };
  }
}
