export default class Institution {
  constructor(id, name, logoAddress, cnpj, email, address, branchActivity) {
    this._id = id || null;
    this._name = name || null;
    this._logoAddress = logoAddress || null;
    this._cnpj = cnpj || null;
    this._email = email || null;
    this._address = address || null;
    this._branchActivity = branchActivity || null;
  }

  montaModel(institution) {
    this._id = institution.id || null;
    this._name = institution.name || null;
    this._logoAddress = institution.logoAddress || null;
    this._cnpj = institution.cnpj || null;
    this._email = institution.email || null;
    this._address = institution.address || null;
    this._branchActivity = institution.branchActivity || null;
  }
  set id(id) {
    this._id = id;
  }

  get id() {
    return this._id;
  }

  set name(name) {
    this._name = name;
  }

  get name() {
    return this._name;
  }

  set logoAddress(logoAddress) {
    this._logoAddress = logoAddress;
  }

  get logoAddress() {
    return this._logoAddress;
  }

  set cnpj(cnpj) {
    this._cnpj = cnpj;
  }

  get cnpj() {
    return this._cnpj;
  }

  set email(email) {
    this._email = email;
  }

  get email() {
    return this._email;
  }

  set address(address) {
    this._address = address;
  }

  get address() {
    return this._address;
  }

  set branchActivity(branchActivity) {
    this._branchActivity = branchActivity;
  }

  get branchActivity() {
    return this._branchActivity;
  }

  get json() {
    return {
      id: this._id || null,
      name: this._name || null,
      logoAddress: this._logoAddress || null,
      cnpj: this._cnpj || null,
      email: this._email || null,
      address: this._address || null,
      branchActivity: this._branchActivity || null
    };
  }
}
