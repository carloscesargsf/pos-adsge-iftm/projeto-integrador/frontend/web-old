export default class SubCategory {
  constructor(id, description, category) {
    this._id = id || null;
    this._description = description || null;
    this._category = category || null;
  }

  montaModel(subCategory) {
    this._id = subCategory.id || null;
    this._description = subCategory.description || null;
    this._category = subCategory.category || null;
  }

  set id(id) {
    this._id = id;
  }

  get id() {
    return this._id;
  }

  set description(description) {
    this._description = description;
  }

  get description() {
    return this._description;
  }

  set category(category) {
    this._category = category;
  }

  get category() {
    return this._category;
  }

  get json() {
    return {
      id: this._id || null,
      description: this._description || null,
      category: this._category || null
    };
  }
}
