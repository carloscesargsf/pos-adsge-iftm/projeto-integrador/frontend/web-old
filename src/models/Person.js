export default class Person {
  constructor(id, name, phone, cpf, email) {
    this._id = id || null;
    this._name = name || null;
    this._phone = phone || null;
    this._cpf = cpf || null;
    this._email = email || null;
  }

  montaModel(person) {
    this._id = person.id || null;
    this._name = person.name || null;
    this._phone = person.phone || null;
    this._cpf = person.cpf || null;
    this._email = person.email || null;
  }
  set id(id) {
    this._id = id;
  }

  get id() {
    return this._id;
  }

  set name(name) {
    this._name = name;
  }

  get name() {
    return this._name;
  }

  set phone(phone) {
    this._phone = phone;
  }

  get phone() {
    return this._phone;
  }

  set cpf(cpf) {
    this._cpf = cpf;
  }

  get cpf() {
    return this._cpf;
  }

  set email(email) {
    this._email = email;
  }

  get email() {
    return this._email;
  }

  get json() {
    return {
      id: this._id || null,
      name: this._name || null,
      phone: this._phone || null,
      cpf: this._cpf || null,
      email: this._email || null
    };
  }
}
