export function isEmpty(string) {
  if (string == null || string == "") {
    return true;
  }
  return false;
}

export function contains(parent, string) {
  if (parent.indexOf(string) >= 0) {
    return true;
  }
  return false;
}
