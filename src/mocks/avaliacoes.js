import questoes from "./questoes.json";

export default function getEvaluations() {
  const evaluations = [];
  for (let i = 0; i < 5; i++) {
    evaluations.push({
      id: i + 1,
      description: "Avaliação teste " + (i + 1),
      questions: getQuestions()
    });
  }
  return evaluations;
}

function getQuestions() {
  const questions = [];
  for (let j = 0; j < 5; j++) {
    questions.push(questoes[j]);
  }
  return questoes;
}
