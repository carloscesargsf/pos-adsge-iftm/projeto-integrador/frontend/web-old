import Vue from "vue";
import router from "./router";
import store from "./store";
import "./registerServiceWorker";
import BootstrapVue from "bootstrap-vue";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";
import Vuelidate from "vuelidate";
import App from "./App.vue";
import Notifications from "vue-notification";

Vue.use(Vuelidate);
Vue.use(BootstrapVue);
Vue.use(Notifications);

Vue.config.productionTip = false;

require("@/assets/main.scss");

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
