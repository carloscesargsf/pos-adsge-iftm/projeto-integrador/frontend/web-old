import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    /**{
      path: "/",
      name: "home",
      component: () => import("@/views/Home/index")
    },*/
    {
      path: "/cadastro-instituicao/basico",
      name: "institution-registry-basic",
      component: () => import("@/views/Institution/Registry/basic")
    },
    {
      path: "/cadastro-instituicao/contato",
      name: "institution-registry-contact",
      component: () => import("@/views/Institution/Registry/contact")
    },
    {
      path: "/cadastro-instituicao/login",
      name: "institution-registry-user",
      component: () => import("@/views/Institution/Registry/user")
    },
    {
      path: "/cadastro-pessoal/dados-pessoais",
      name: "person-registry-personal-data",
      component: () => import("@/views/Person/Registry/personalData")
    },
    {
      path: "/cadastro-pessoal/login",
      name: "person-registry-user",
      component: () => import("@/views/Person/Registry/user")
    },
    {
      path: "/cadastro-questao",
      name: "questions-registry",
      component: () => import("@/views/Question/Registry/form")
    },
    {
      path: "/cadastro-avaliacao",
      name: "evaluations-registry",
      component: () => import("@/views/Evaluation/Registry/form")
    },
    {
      path: "/lista-avaliacao",
      name: "evaluations-list",
      component: () => import("@/views/Evaluation/List/list")
    },
    {
      path: "/minhas-avaliacoes",
      name: "evaluations-list-me",
      component: () => import("@/views/Evaluation/List/list")
    },
    {
      path: "/cadastro-categoria",
      name: "categories-registry",
      component: () => import("@/views/Category/Registry/form")
    },
    {
      path: "/lista-categoria",
      name: "categories-list",
      component: () => import("@/views/Category/List/list")
    }
  ]
});
